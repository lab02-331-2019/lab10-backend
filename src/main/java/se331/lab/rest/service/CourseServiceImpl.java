package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;

import java.util.List;

@Service
@Slf4j
public class CourseServiceImpl implements CourseService{
    @Autowired
    CourseDao courseDao;

    @Override
    public List<Course> getAllCourse() {
        List<Course> courses = courseDao.getAllCourse();
        return courses;
    }

    @Override
    public Course findById(Long id) {
        return courseDao.findById(id);
    }

    @Override
    public Course saveCourse(Course course) {
        return courseDao.saveCourse(course);
    }

}
