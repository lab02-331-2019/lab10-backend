package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.repository.LecturerRepository;
import se331.lab.rest.service.LecturerService;

import java.util.List;

@Repository
@Profile("lecturerDBDao")
@Slf4j
public class LecturerDBDaoImpl implements LecturerDao {
    @Autowired
    LecturerRepository lecturerRepository;

    @Override
    public List<Lecturer> getAllLecturer() {
        return lecturerRepository.findAll();
    }

    @Override
    public Lecturer findById(Long id) {
        return lecturerRepository.findById(id).orElse(null);

    }

    @Override
    public Lecturer saveLecturer(Lecturer lecturer) {
        return lecturerRepository.save(lecturer);

    }
}
